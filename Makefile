
R = R
PANDOC = pandoc
MKDIR = mkdir -p
RM = rm -f

package = poibm
version := $(shell sed -n 's@^Version: \(.*\)$$@\1@p' r/$(package)/DESCRIPTION)
arch := $(shell $(R) CMD Rscript -e 'cat(R.version$$platform)')

srcpkg = $(package)_$(version).tar.gz
binpkg = $(package)_$(version)_R_$(arch).tar.gz

ifeq "$(findstring apple-darwin,$(arch))" "apple-darwin"
binpkg = $(package)_$(version).tgz
endif

all: srcpkg binpkg

clean:
	$(RM) $(srcpkg) $(binpkg)

srcpkg: $(srcpkg)

$(srcpkg):
	$(R) CMD build r/$(package)/

binpkg: $(binpkg)

$(binpkg): $(srcpkg)
	$(MKDIR) -p ./.Rlibrary/
	$(R) CMD INSTALL --library=./.Rlibrary/ --build $(srcpkg)

install: $(binpkg)
	$(R) CMD INSTALL $(binpkg)

uninstall:
	$(R) CMD REMOVE $(package)

README.html: README.md
	$(PANDOC) -o $@ $<

.PHONY: all clean $(srcpkg) srcpkg binpkg install uninstall
.SUFFIXES:
