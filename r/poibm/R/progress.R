
#
# progress bar utilities, internal API
#

#. set up the progress bar line
.poibm.progress.init <- function(max.resets, max.iter) {
	# set up field widths
	wr <- nchar(sprintf('%d', max.resets))
	wi <- nchar(sprintf('%d', max.iter))
	wp <- 40L

	# get current time
	ts <- proc.time()[['elapsed']]

	# wrap up
	return (list( max.resets = max.resets, max.iter = max.iter,
		used = 0L, nlogl = Inf, wp = wp,
		ts.start = ts, ts.last = -Inf, ts.gap = 1/25.,
		tty = stderr(),
		format = sprintf('POIBM: |%%-%ds| R:%%%dd/%%d I:%%%dd/%%d O:%%8.2e(%%-9.2e) %%s', wp, wr, wi) ))
}

#. update the progress bar line with raw text
.poibm.progress.put <- function(progress, str) {
	# string to clear previous string
	rub <- strrep('\b', progress$used)
	# string to overwrite previous string
	blank <- strrep(' ', progress$used)

	# emit data
	cat(paste(c(rub, blank, rub, str), collapse = ''), file = progress$tty)
	# update rub counter
	progress$used = nchar(str)

	return (progress)
}

#. update the progress bar line
.poibm.progress.update <- function(progress, reset, iter, nlogl, message = '', always = F) {
	# update if done or time elapsed...
	ts <- proc.time()[['elapsed']]
	if (always || ts - progress$ts.last > progress$ts.gap) {
		# get fractional progress
		frac <- ( ( reset-1L )*progress$max.iter + iter ) / ( progress$max.resets * progress$max.iter )
		stars <- strrep('#', round(frac * progress$wp))

		# update message
		progress <- .poibm.progress.put(progress,
			sprintf(progress$format, stars, reset, progress$max.resets,
				iter, progress$max.iter, nlogl, nlogl - progress$nlogl, message))

		# update current
		progress$nlogl <- nlogl
		progress$ts.last <- ts
	}

	return (progress)
}

#. finish using the progress bar
.poibm.progress.deinit <- function(progress) {
	# emit data
	cat('\n', file = progress$tty)
}
