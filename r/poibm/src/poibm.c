
/* ---- heavy lifting done in plain C ---- */

#include <alloca.h>
#include <assert.h>
#include <math.h>

#pragma omp declare simd
static
double
safe_mul(double x, double y)
{
	/* Multiplication, left is strong, 0*y = 0 */
	if (x > 0.)
		return x * y;
	else
		return 0.;
}

#pragma omp declare simd
static
double
safe_div(double x, double y)
{
	/* Division, left is strong, 0/y = 0 */
	if (x > 0.)
		return x / y;
	else
		return 0.;
}

#pragma omp declare simd
static
double
safe_xlogy(double x, double y)
{
	/* Cross-entropy, left is strong, 0*log(y) = 0 */
	if (x > 0.)
		return x * log(y);
	else
		return 0.;
}

/*
 * these update equations are straight out of the paper.. just push the
 * redundant stuff out of each loop to save FLOPs..
 */

static
void
poibm_upd_u(const double *X, const double *Y, const double *c, double *u, const double *v_X, const double *v_Y, const double *w, size_t m, size_t nx, size_t ny, double small)
{
	size_t j;

	/* Each sample on Y */
#pragma omp parallel for schedule(static) if(ny > 1)
	for (j = 0; j < ny; ++j)
	{
		double s, t, a, b;

		/* Get total weight */
		s = 0.;
		{{ size_t k;
#pragma omp simd reduction(+: s)
		for (k = 0; k < nx; ++k)
			s += w[k + j*nx];
		}}

		/* Accumulate model, constant part */
		t = 0.;
		{{ size_t k;
#pragma omp simd reduction(+: t)
		for (k = 0; k < nx; ++k)
			t += w[k + j*nx] * v_X[k];
		}}

		/* Each gene */
		{{ size_t i;
		for (i = 0; i < m; ++i)
		{
			/* Accumulate data */
			a = 0.;
			{{ size_t k;
#pragma omp simd reduction(+: a)
			for (k = 0; k < nx; ++k)
				a += w[k + j*nx] * X[i + k*m];
			}}

			/* Get model, gene specific part */
			b = t * c[i];

			/* ML solve */
			u[i + j*m] = safe_div( safe_div(a, s) + Y[i + j*m], safe_div(b, s) + v_Y[j] );
		}
		}}
	}

	/* No need for small */
	(void)small;
}

static
void
poibm_upd_v_X(const double *X, const double *Y, const double *c, const double *u, double *v_X, const double *v_Y, const double *w, size_t m, size_t nx, size_t ny, double small)
{
	double *ty;

	/* Get scratch */
	ty = (double *)alloca(ny * sizeof(*ty));

	/* Summon models, constant part */
	{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1)
	for (j = 0; j < ny; ++j)
	{
		double s;

		s = 0.;
		{{ size_t i;
#pragma omp simd reduction(+: s)
		for (i = 0; i < m; ++i)
			s += c[i] * u[i + j*m];
		}}

		ty[j] = s;
	}
	}}

	/* Each sample on X */
	{{ size_t k;
#pragma omp parallel for schedule(static) if(nx > 1)
	for (k = 0; k < nx; ++k)
	{
		double s, a, b;

		/* Accumulate data, value part */
		s = 0.;
		{{ size_t i;
#pragma omp simd reduction(+: s)
		for (i = 0; i < m; ++i)
			s += X[i + k*m];
		}}

		/* Accumulate data, weight part */
		a = 0.;
		{{ size_t j;
#pragma omp simd reduction(+: a)
		for (j = 0; j < ny; ++j)
			a += fmax( w[k + j*nx], small );
		}}

		/* Accumulate model */
		b = 0.;
		{{ size_t j;
#pragma omp simd reduction(+: b)
		for (j = 0; j < ny; ++j)
			b += fmax( w[k + j*nx], small ) * ty[j];
		}}

		/* ML solve */
		v_X[k] = safe_div(a * s, b);
	}
	}}

	/* No need for Y this time */
	(void)Y, (void)v_Y;
}

static
void
poibm_upd_v_Y(const double *X, const double *Y, const double *c, const double *u, const double *v_X, double *v_Y, const double *w, size_t m, size_t nx, size_t ny, double small)
{
	/* Each sample on Y */
	{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1)
	for (j = 0; j < ny; ++j)
	{
		double a, b;

		/* Accumulate data */
		a = 0.;
		{{ size_t i;
#pragma omp simd reduction(+: a)
		for (i = 0; i < m; ++i)
			a += Y[i + j*m];
		}}

		/* Accumulate model */
		b = 0.;
		{{ size_t i;
#pragma omp simd reduction(+: b)
		for (i = 0; i < m; ++i)
			b += u[i + j*m];
		}}

		/* ML solve */
		v_Y[j] = safe_div(a, b);
	}
	}}

	/* No need for Y this time */
	(void)X, (void)c, (void)v_X, (void)w, (void)nx;
	/* No need for small */
	(void)small;
}

static
void
poibm_upd_c(const double *X, const double *Y, double *c, const double *u, const double *v_X, const double *v_Y, const double *w, size_t m, size_t nx, size_t ny, double small)
{
	double *tx, *ty;

	/* Get scratch */
	tx = (double *)alloca(nx * sizeof(*tx));
	ty = (double *)alloca(ny * sizeof(*ty));

	/* Get data weight, constant part */
	{{ size_t k;
#pragma omp parallel for schedule(static) if(nx > 1)
	for (k = 0; k < nx; ++k)
	{
		double s;

		/* TODO: transpose loop? */

		s = 0.;
		{{ size_t j;
#pragma omp simd reduction(+: s)
		for (j = 0; j < ny; ++j)
			s += fmax( w[k + j*nx], small );
		}}

		tx[k] = s;
	}
	}}

	/* Summon model, constant part */
	{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1)
	for (j = 0; j < ny; ++j)
	{
		double s;

		s = 0.;
		{{ size_t k;
#pragma omp simd reduction(+: s)
		for (k = 0; k < nx; ++k)
			s += fmax( w[k + j*nx], small ) * v_X[k];
		}}

		ty[j] = s;
	}
	}}

	/* For each gene */
	{{ size_t i;
#pragma omp parallel for schedule(static) if(m > 1)
	for (i = 0; i < m; ++i)
	{
		double a, b;

		/* Accumulate data */
		a = 0.;
		{{ size_t k;
#pragma omp simd reduction(+: a)
		for (k = 0; k < nx; ++k)
			a += tx[k] * X[i + k*m];
		}}

		/* Accumulate model */
		b = 0.;
		{{ size_t j;
#pragma omp simd reduction(+: b)
		for (j = 0; j < ny; ++j)
			b += ty[j] * u[i + j*m];
		}}

		/* ML solve */
		c[i] = safe_div( a, b );
	}
	}}

	/* No need for Y this time */
	(void)Y, (void)v_Y;
}

static
void
poibm_init_w(const double *X, const double *Y, const double *c, const double *u, const double *v_X, const double *v_Y, double *w, double *off_X, double *off_Y, size_t m, size_t nx, size_t ny)
{
	double *tx, *ty;

	/* Allocate scratch */
	tx = (double *)alloca(nx * sizeof(*tx));
	ty = (double *)alloca(ny * sizeof(*ty));

	/* Compute scaling factors for X */
	{{ size_t k;
#pragma omp parallel for schedule(static) if(nx > 1)
	for (k = 0; k < nx; ++k)
	{
		size_t i;
		double s;

		/* Accumulate */
		s = 0.;
#pragma omp simd reduction(+: s)
		for (i = 0; i < m; ++i)
			s += X[i + k*m];

		/* Store */
		tx[k] = s;
	}
	}}

	/* Compute scaling factors for Y */
	{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1)
	for (j = 0; j < ny; ++j)
	{
		size_t i;
		double s;

		/* Accumulate */
		s = 0.;
#pragma omp simd reduction(+: s)
		for (i = 0; i < m; ++i)
			s += c[i] * Y[i + j*m];

		/* Store */
		ty[j] = s;
	}
	}}

	/* Compute weights */
	{{ size_t k_j;
#pragma omp parallel for schedule(static) if(nx*ny > 1)
	for (k_j = 0; k_j < nx*ny; ++k_j)
	{
		size_t j, k, i;
		double s;

		/* Get indices */
		k = k_j % nx;
		j = k_j / nx;

		/* Accumulate evidence */
		s = 0.;
#pragma omp simd reduction(+: s)
		for (i = 0; i < m; ++i)
		{
			double sx, sy, lam, lam_x, lam_y;

			/* Get models */
			sx = c[i] * tx[k];
			sy = ty[j];
			lam = safe_div( X[i + k*m] + Y[i + j*m], sx + sy );
			lam_x = sx * lam;
			lam_y = sy * lam;

			/* Get likelihood */
			s += safe_xlogy( X[i + k*m], lam_x ) + safe_xlogy( Y[i + j*m], lam_y );
		}

		/* Store */
		w[k + j*nx] = s + off_X[k] + off_Y[j];
	}
	}}

	/* Not used, we re-estimate above */
	(void)u, (void)v_X, (void)v_Y;
}

static
void
poibm_cor_w(double *w, size_t nx, size_t ny)
{
	size_t j;

	/* Loop through */
#pragma omp parallel for schedule(static) if(ny > 1)
	for (j = 0; j < ny; ++j)
	{
		size_t k;

#pragma omp simd
		for (k = 0; k < nx; ++k)
			/* Add small-distance correction */
			w[k + j*nx] += -log1p(exp( w[k + j*nx] ));
	}
}

/*
 * now to pick the top samples from each set.. do a quickselect on each
 * dimension in the order of the log weights..
 */

static
int
qs_less(const double *w, size_t J1, size_t J2)
{
	/* Sorts the indices J1, J2 to the order of decreasing weight */

#if 0
	/* Stable sort */
	if (w[J1] == w[J2])
		return J1 < J2;
	else
		return w[J1] > w[J2];

#else
	/* Unstable sort */
	return w[J1] > w[J2];

#endif
}

static
size_t
qs_partition(const double *w, size_t *J, size_t n)
{
	assert(n > 0);

	/* Do a median of three */
	{{ size_t a, c, b, t;

	a = 0;
	c = n-1;
	b = a+(c-a)/2;

	if (qs_less(w, J[b], J[a]))
	{ t = J[a]; J[a] = J[b]; J[b] = t; }

	if (qs_less(w, J[c], J[a]))
	{ t = J[a]; J[a] = J[c]; J[c] = t; }
	if (qs_less(w, J[b], J[c]))
	{ t = J[b]; J[b] = J[c]; J[c] = t; }

	}}

	/* Partition about that */
	{{ size_t p, q, J_q;

	p = 0;
	q = n-1;

	J_q = J[q];

	for (; p+1 < q;)
	{
		if (qs_less(w, J_q, J[p]))
		{
			J[q] = J[p];
			--q;
			J[p] = J[q];
		}
		else
			++p;

		if (!qs_less(w, J[p], J_q))   /* NB. alt side on ties, no O(n^2) wc thx */
		{
			J[q] = J[p];
			--q;
			J[p] = J[q];
		}
		else
			++p;
	}
	if (p < q)
	{
		if (qs_less(w, J_q, J[p]))
		{
			J[q] = J[p];
			--q;
			J[p] = J[q];
		}
		else
			++p;
	}

	J[q] = J_q;

	return q;
	}}
}

static
void
mark_used(size_t *c, size_t *h, size_t i)
{
	/* Maintain a hit counter h[i] and coverage c of nonzeros */
	if (h[i]++ == 0)
		++*c;
}

static
void
mark_unused(size_t *c, size_t *h, size_t i)
{
	/* Roll back a hit */
	if (--h[i] == 0)
		--*c;
}

static
size_t
marks_update_X(size_t *cx, size_t *hx, const size_t *J, size_t nx, size_t q, size_t p)
{
	/* Add stuff going right */
	for (; q < p; ++q)
		mark_used(cx, hx, J[q] % nx);
	/* Drop stuff going left */
	for (; p < q; --q)
		mark_unused(cx, hx, J[q-1] % nx);

	return q;
}

static
size_t
marks_update_Y(size_t *cy, size_t *hy, const size_t *J, size_t nx, size_t q, size_t p)
{
	/* Add stuff going right */
	for (; q < p; ++q)
		mark_used(cy, hy, J[q] / nx);
	/* Drop stuff going left */
	for (; p < q; --q)
		mark_unused(cy, hy, J[q-1] / nx);

	return q;
}

static
size_t
select_inds_X(size_t *cx, size_t *hx, const double *w, size_t *J, size_t nx, size_t ny, size_t ux)
{
	size_t l, u, q, p;

	/* Set up bounds */
	l = 0;
	u = nx * ny;
	/* Set up state pointer */
	q = 0;

	/* Search */
	while (l < u)
	{
		/* Partition & update state */
		p = l + qs_partition(w, &J[l], u-l);
		q = marks_update_X(cx, hx, J, nx, q, p);

		/* Branch */
		if (!(*cx >= ux))
			l = p + 1;
		else
			u = p;
	}

		/* Update state */
		q = marks_update_X(cx, hx, J, nx, q, l);

	return l;
}

static
size_t
select_inds_Y(size_t *cy, size_t *hy, const double *w, size_t *J, size_t nx, size_t ny, size_t uy)
{
	size_t l, u, q, p;

	/* Set up bounds */
	l = 0;
	u = nx * ny;
	q = 0;

	/* Search */
	while (l < u)
	{
		/* Partition & update */
		p = l + qs_partition(w, &J[l], u-l);
		q = marks_update_Y(cy, hy, J, nx, q, p);

		/* Branch */
		if (!(*cy >= uy))
			l = p + 1;
		else
			u = p;
	}

		/* Update */
		q = marks_update_Y(cy, hy, J, nx, q, l);

	return l;
}

static
void
poibm_sel_w(size_t *cx, size_t *cy, size_t *hx, size_t *hy, const double *w, size_t *J, size_t nx, size_t ny, size_t ux, size_t uy)
{
	/* Set up linear indices */
	{{ size_t k_j;
#pragma omp simd
	for (k_j = 0; k_j < nx*ny; ++k_j)
		J[k_j] = k_j;
	}}

	/* Set up state for X */
	*cx = *cy = 0;
	{{ size_t k;
#pragma omp simd
	for (k = 0; k < nx; ++k)
		hx[k] = 0;
	}}

	/* Sort for X */
	select_inds_X(cx, hx, w, J, nx, ny, ux);

	/* Set up state for Y */
	{{ size_t j;
#pragma omp simd
	for (j = 0; j < ny; ++j)
		hy[j] = 0;
	}}

	/* Sort for Y */
	select_inds_Y(cy, hy, w, J, nx, ny, uy);
}

#pragma omp declare simd
static
double
logsumexp(double x, double y)
{
	if (!(y > x))
		return x + log1p(exp(y - x));
	else
		return y + log1p(exp(x - y));
}

#pragma omp declare reduction \
	(logsumexp : double : omp_out = logsumexp(omp_out, omp_in)) \
	initializer( omp_priv = -HUGE_VAL )

static
double
poibm_scal_w(double *w, size_t nx, size_t ny, size_t orig_nx, size_t orig_ny, size_t max_iter)
{
	double *r, *c, *sr, sr0, sc0;

	/* Allocate scratch */
	r = (double *)alloca(nx * sizeof(*r));
	c = (double *)alloca(ny * sizeof(*c));
	sr = (double *)alloca(nx * sizeof(*sr));

	/* Set up normalizers */
	{{ size_t k;
#pragma omp simd
	for (k = 0; k < nx; ++k)
		r[k] = 0.;
	}}
	{{ size_t j;
#pragma omp simd
	for (j = 0; j < ny; ++j)
		c[j] = 0.;
	}}

	/* Precompute offsets */
	sr0 = log( (double)orig_ny );
	sc0 = log( (double)orig_nx );

	/* For each pass */
	{{ size_t iter;
	for (iter = 0; iter < max_iter; ++iter)
	{
		/* Normalize rows */
		{{
			/* Set up */
			{{ size_t k;
#pragma omp simd
			for (k = 0; k < nx; ++k)
				sr[k] = -HUGE_VAL;
			}}

			/* Get normalizer */
			{{ size_t j;
/* TODO: mt */
			for (j = 0; j < ny; ++j)
				{{ size_t k;
#pragma omp simd
				for (k = 0; k < nx; ++k)
					sr[k] = logsumexp( sr[k], w[k + j*nx] );
				}}
			}}

			/* Apply */
			{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1)
			for (j = 0; j < ny; ++j)
				{{ size_t k;
#pragma omp simd
				for (k = 0; k < nx; ++k)
					w[k + j*nx] += sr0 - sr[k];
				}}
			}}

			/* Store */
			{{ size_t k;
#pragma omp simd
			for (k = 0; k < nx; ++k)
				r[k] += sr0 - sr[k];
			}}
		}}

		/* Normalize columns */
		{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1)
		for (j = 0; j < ny; ++j)
		{
			double sc, dc;

			/* Get normalizer */
			sc = -HUGE_VAL;
			{{ size_t k;
#pragma omp simd reduction(logsumexp: sc)
			for (k = 0; k < nx; ++k)
				sc = logsumexp( sc, w[k + j*nx] );
			}}

			/* Apply */
			dc = sc0 - sc;
			{{ size_t k;
#pragma omp simd
			for (k = 0; k < nx; ++k)
				w[k + j*nx] += dc;
			}}

			/* Save it */
			c[j] += dc;
		}
		}}
	}
	}}

	/* Sum up likelihood & transform to probabilities */
	{{ double logl;

	/* Sum 'em up */
	logl = 0.;
	{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1) reduction(+: logl)
	for (j = 0; j < ny; ++j)
	{
		{{ size_t k;
#pragma omp simd reduction(+: logl)
		for (k = 0; k < nx; ++k)
		{
			double pr, logl_kj;

			/* Grab values */
			pr = exp( w[k + j*nx] );
			logl_kj = w[k + j*nx] - ( r[k] + c[j] );

			/* Apply */
			logl+= safe_mul(pr, logl_kj);
			w[k + j*nx] = pr;
		}
		}}
	}
	}}

	return -logl;
	}}
}

static
void
poibm_map_canon(double *c, double *u, double *v_X, double *v_Y, size_t m, size_t nx, size_t ny) 
{
	double sc, sx, sy, mx, my, mc;

	/* Get magnitude of the batch effects c */
	sc = 0.;
	{{ size_t i;
#pragma omp simd reduction(+: sc)
	for (i = 0; i < m; ++i)
		sc += c[i];
	}}

	/* Get magnitude of X */
	sx = 0.;
	{{ size_t k;
#pragma omp simd reduction(+: sx)
	for (k = 0; k < nx; ++k)
		sx += v_X[k];
	}}

	/* Get magnitude of Y */
	sy = 0.;
	{{ size_t j;
#pragma omp simd reduction(+: sy)
	for (j = 0; j < ny; ++j)
		sy += v_Y[j];
	}}

	/* Solve scale factors */
	mx = safe_div( sc*sx + m*sy , sc*(nx+ny) );
	my = safe_div( sc*sx + m*sy ,  m*(nx+ny) );
	mc = sc / m;

	/* Scale batch effects */
	{{ size_t i;
#pragma omp simd
	for (i = 0; i < m; ++i)
		c[i] = safe_div( c[i], mc );
	}}

	/* Scale X gains */
	{{ size_t k;
	for (k = 0; k < nx; ++k)
		v_X[k] = safe_div( v_X[k], mx );
	}}

	/* Scale Y gains */
	{{ size_t j;
	for (j = 0; j < ny; ++j)
		v_Y[j] = safe_div( v_Y[j], my );
	}}

	/* Scale profiles */
	{{ size_t j;
#pragma omp parallel for schedule(static) if(ny > 1)
	for (j = 0; j < ny; ++j)
	{
		size_t i;

#pragma omp simd
		for (i = 0; i < m; ++i)
			u[i + j*m] *= my;
	}
	}}
}

/* ---- R wrappers ---- */

#include <Rinternals.h>

static
int
is_matrix(SEXP x_R)
{ return isVector(x_R) || isMatrix(x_R); }

static
int
is_col_vector(SEXP x_R)
{ return isVector(x_R) || ( isMatrix(x_R) && ncols(x_R) == 1 ); }

static
int
is_row_vector(SEXP x_R)
{ return isVector(x_R) || ( isMatrix(x_R) && nrows(x_R) == 1 ); }

static
int
is_vector(SEXP x_R)
{ return is_col_vector(x_R) || is_row_vector(x_R); }

SEXP
poibm_safe_mul_R(SEXP x_R, SEXP y_R)
{
	SEXP r_R;
	int n, j;

	/* Check input */
	if (!(isReal(x_R)))
		error("'%s' must be a double array", "x");
	if (!(isReal(y_R) && length(y_R) == length(x_R)))
		error("'%s' must be a compatible double array", "y");

	/* Create output */
	n = length(x_R);
	r_R = PROTECT(duplicate(x_R));

	/* Compute */
#pragma omp simd
	for (j = 0; j < n; ++j)
		REAL(r_R)[j] = safe_mul(REAL(x_R)[j], REAL(y_R)[j]);

	UNPROTECT(1);
	return r_R;
}

SEXP
poibm_safe_div_R(SEXP x_R, SEXP y_R)
{
	SEXP r_R;
	int n, j;

	/* Check input */
	if (!(isReal(x_R)))
		error("'%s' must be a double array", "x");
	if (!(isReal(y_R) && length(y_R) == length(x_R)))
		error("'%s' must be a compatible double array", "y");

	/* Create output */
	n = length(x_R);
	r_R = PROTECT(duplicate(x_R));

	/* Compute */
#pragma omp simd
	for (j = 0; j < n; ++j)
		REAL(r_R)[j] = safe_div(REAL(x_R)[j], REAL(y_R)[j]);

	UNPROTECT(1);
	return r_R;
}

SEXP
poibm_safe_xlogy_R(SEXP x_R, SEXP y_R)
{
	SEXP r_R;
	int n, j;

	/* Check inputs */
	if (!(isReal(x_R)))
		error("'%s' must be a double array", "x");
	if (!(isReal(y_R) && length(y_R) == length(x_R)))
		error("'%s' must be a compatible double array", "y");

	/* Create output */
	n = length(x_R);
	r_R = PROTECT(duplicate(x_R));

	/* Compute */
#pragma omp simd
	for (j = 0; j < n; ++j)
		REAL(r_R)[j] = safe_xlogy(REAL(x_R)[j], REAL(y_R)[j]);

	UNPROTECT(1);
	return r_R;
}

SEXP
poibm_diag_mull_R(SEXP d_R, SEXP A_R)
{
	int m, n;
	SEXP R_R;

	/* Check inputs */
	if (!(isReal(d_R) && is_vector(d_R)))
		error("'%s' msut be a double vector", "d");
	if (!(isReal(A_R) && is_matrix(A_R)))
		error("'%s' must be a double matrix", "A");

	/* Get dimensions */
	m = nrows(A_R);
	n = ncols(A_R);

	/* Check dimensions */
	if (!(nrows(A_R) == m && ncols(A_R) == n))
		error("'%s' must be %d-by-%d", "A", m, n);
	if (!(length(d_R) == m))
		error("'%s' must be %d-by-%d", "d", m, 1);

	/* Create output */
	R_R = PROTECT(duplicate(A_R));

	/* Compute */
	{{ const double *d, *A;
	double *R;
	int j;
	d = REAL(d_R);   /* NB. REAL() not reentrant !!! */
	A = REAL(A_R);
	R = REAL(R_R);
#pragma omp parallel for schedule(static) if(n > 1)
	for (j = 0; j < n; ++j)
	{
		int i;

#pragma omp simd
		for (i = 0; i < m; ++i)
			R[i + j*m] = d[i] * A[i + j*m];
	}
	}}

	UNPROTECT(1);
	return R_R;
}

SEXP
poibm_diag_mulr_R(SEXP A_R, SEXP d_R)
{
	int m, n;
	SEXP R_R;

	/* Check inputs */
	if (!(isReal(A_R) && is_matrix(A_R)))
		error("'%s' must be a double matrix", "A");
	if (!(isReal(d_R) && is_vector(d_R)))
		error("'%s' must be a double vector", "d");

	/* Get dimensions */
	m = nrows(A_R);
	n = ncols(A_R);

	/* Check dimensions */
	if (!(nrows(A_R) == m && ncols(A_R) == n))
		error("'%s' must be %d-by-%d", "A", m, n);
	if (!(length(d_R) == n))
		error("'%s' must be %d-by-%d", "d", n, 1);

	/* Create output */
	R_R = PROTECT(duplicate(A_R));

	/* Compute */
	{{ const double *d, *A;
	double *R;
	int j;
	d = REAL(d_R);
	A = REAL(A_R);
	R = REAL(R_R);
#pragma omp parallel for schedule(static) if(n > 1)
	for (j = 0; j < n; ++j)
	{
		int i;

#pragma omp simd
		for (i = 0; i < m; ++i)
			R[i + j*m] = A[i + j*m] * d[j];
	}
	}}

	UNPROTECT(1);
	return R_R;
}

static
void
check_params(size_t *p_m, size_t *p_nx, size_t *p_ny, SEXP c_R, SEXP u_R, SEXP v_X_R, SEXP v_Y_R, SEXP w_R)
{
	int m, nx, ny;

	/* Check input types */
	if (!(isReal(c_R) && is_col_vector(c_R)))
		error("'%s' must be a double column vector", "c");
	if (!(isReal(u_R) && is_matrix(u_R)))
		error("'%s' must be a double matrix", "u");
	if (!(isReal(v_X_R) && is_row_vector(v_X_R)))
		error("'%s' must be a double row vector", "v.X");
	if (!(isReal(v_Y_R) && is_row_vector(v_Y_R)))
		error("'%s' must be a double row vector", "v.Y");
	if (!(isReal(w_R) && is_matrix(w_R)))
		error("'%s' must be a double matrix", "w");

	/* Get dimensions */
	m = length(c_R);
	nx = length(v_X_R);
	ny = length(v_Y_R);

	/* Check dimensions */
	if (!(length(c_R) == m))
		error("'%s' must be %d-by-%d", "c", m, 1);
	if (!(nrows(u_R) == m && ncols(u_R) == ny))
		error("'%s' must be %d-by-%d", "u", m, ny);
	if (!(length(v_X_R) == nx))
		error("'%s' must be %d-by-%d", "v.X", 1, nx);
	if (!(length(v_Y_R) == ny))
		error("'%s' must be %d-by-%d", "v.Y", 1, ny);

	/* Upcast */
	*p_m = (size_t)m;
	*p_nx = (size_t)nx;
	*p_ny = (size_t)ny;
}

static
void
check_data(SEXP X_R, size_t m, size_t n, const char *X_name)
{
	/* Check input types */
	if (!(isReal(X_R) && is_matrix(X_R)))
		error("'%s' must be a double matrix", X_name);

	/* Check dimensions */
	if (!(nrows(X_R) == (int)m && ncols(X_R) == (int)n))
		error("'%s' must be %d-by-%d", X_name, m, n);
}

static
void
check_w(size_t *p_nx, size_t *p_ny, SEXP w_R)
{
	int nx, ny;

	/* Check input types */
	if (!(isReal(w_R) && is_matrix(w_R)))
		error("'%s' must be a double matrix", "w");

	/* Get dimensions */
	nx = nrows(w_R);
	ny = ncols(w_R);

	/* Check dimensions */
	if (!(nrows(w_R) == nx && ncols(w_R) == ny))
		error("'%s' must be %d-by-%d", "w", nx, ny);

	/* Upcast */
	*p_nx = (size_t)nx;
	*p_ny = (size_t)ny;
}

static
void
check_inds(size_t *u, SEXP inds_R, size_t n, const char *inds_name)
{
	/* Check input types */
	if (!(isInteger(inds_R) && is_vector(inds_R)))
		error("'%s' must be an integer vector", inds_name);

	/* Get size */
	*u = (size_t)length(inds_R);

	/* Check data */
	{{
	size_t i;
	int last_v;

	last_v = 0;
	for (i = 0; i < *u; ++i)
	{
		if (!(INTEGER(inds_R)[i] > last_v))
			error("'%s' must be a strictly monotonic sequence of %d:%d", inds_name, 1, (int)n);

		last_v = INTEGER(inds_R)[i];
	}
	}}
}

SEXP
poibm_upd_u_R(SEXP X_R, SEXP Y_R, SEXP c_R, SEXP u_R, SEXP v_X_R, SEXP v_Y_R, SEXP w_R, SEXP small_R)
{
	size_t m, nx, ny;

	/* Check arguments */
	check_params(&m, &nx, &ny, c_R, u_R, v_X_R, v_Y_R, w_R);
	check_data(X_R, m, nx, "X");
	check_data(Y_R, m, ny, "Y");

	/* Check small */
	if (!(isReal(small_R) && length(small_R) == 1))
		error("'%s' must be a double scalar", "small");

	/* Update */
	poibm_upd_u(REAL(X_R), REAL(Y_R), REAL(c_R), REAL(u_R), REAL(v_X_R), REAL(v_Y_R), REAL(w_R), m, nx, ny, *REAL(small_R));

	return u_R;
}

SEXP
poibm_upd_v_X_R(SEXP X_R, SEXP Y_R, SEXP c_R, SEXP u_R, SEXP v_X_R, SEXP v_Y_R, SEXP w_R, SEXP small_R)
{
	size_t m, nx, ny;

	/* Check arguments */
	check_params(&m, &nx, &ny, c_R, u_R, v_X_R, v_Y_R, w_R);
	check_data(X_R, m, nx, "X");
	check_data(Y_R, m, ny, "Y");

	/* Check small */
	if (!(isReal(small_R) && length(small_R) == 1))
		error("'%s' must be a double scalar", "small");

	/* Update */
	poibm_upd_v_X(REAL(X_R), REAL(Y_R), REAL(c_R), REAL(u_R), REAL(v_X_R), REAL(v_Y_R), REAL(w_R), m, nx, ny, *REAL(small_R));

	return v_X_R;
}

SEXP
poibm_upd_v_Y_R(SEXP X_R, SEXP Y_R, SEXP c_R, SEXP u_R, SEXP v_X_R, SEXP v_Y_R, SEXP w_R, SEXP small_R)
{
	size_t m, nx, ny;

	/* Check arguments */
	check_params(&m, &nx, &ny, c_R, u_R, v_X_R, v_Y_R, w_R);
	check_data(X_R, m, nx, "X");
	check_data(Y_R, m, ny, "Y");

	/* Check small */
	if (!(isReal(small_R) && length(small_R) == 1))
		error("'%s' must be a double scalar", "small");

	/* Update */
	poibm_upd_v_Y(REAL(X_R), REAL(Y_R), REAL(c_R), REAL(u_R), REAL(v_X_R), REAL(v_Y_R), REAL(w_R), m, nx, ny, *REAL(small_R));

	return v_Y_R;
}

SEXP
poibm_upd_c_R(SEXP X_R, SEXP Y_R, SEXP c_R, SEXP u_R, SEXP v_X_R, SEXP v_Y_R, SEXP w_R, SEXP small_R)
{
	size_t m, nx, ny;

	/* Check arguments */
	check_params(&m, &nx, &ny, c_R, u_R, v_X_R, v_Y_R, w_R);
	check_data(X_R, m, nx, "X");
	check_data(Y_R, m, ny, "Y");

	/* Check small */
	if (!(isReal(small_R) && length(small_R) == 1))
		error("'%s' must be a double scalar", "small");

	/* Update */
	poibm_upd_c(REAL(X_R), REAL(Y_R), REAL(c_R), REAL(u_R), REAL(v_X_R), REAL(v_Y_R), REAL(w_R), m, nx, ny, *REAL(small_R));

	return c_R;
}

SEXP
poibm_init_w_R(SEXP X_R, SEXP Y_R, SEXP c_R, SEXP u_R, SEXP v_X_R, SEXP v_Y_R, SEXP w_R, SEXP off_X_R, SEXP off_Y_R)
{
	size_t m, nx, ny;

	/* Check arguments */
	check_params(&m, &nx, &ny, c_R, u_R, v_X_R, v_Y_R, w_R);
	check_data(X_R, m, nx, "X");
	check_data(Y_R, m, ny, "Y");

	/* Check offsets */
	if (!(isReal(off_X_R) && is_col_vector(off_X_R) && length(off_X_R) == (int)nx))
		error("'%s' must be a %d-by-%d double column vector", "off.X", (int)nx, (int)1);  
	if (!(isReal(off_Y_R) && is_col_vector(off_Y_R) && length(off_Y_R) == (int)ny))
		error("'%s' must be a %d-by-%d double column vector", "off.Y", (int)ny, (int)1);  

	/* Update */
	poibm_init_w(REAL(X_R), REAL(Y_R), REAL(c_R), REAL(u_R), REAL(v_X_R), REAL(v_Y_R), REAL(w_R), REAL(off_X_R), REAL(off_Y_R), m, nx, ny);

	return w_R;
}

SEXP
poibm_cor_w_R(SEXP w_R)
{
	size_t nx, ny;

	/* Check arguments */
	check_w(&nx, &ny, w_R);

	/* Update */
	poibm_cor_w(REAL(w_R), nx, ny);

	return w_R;
}

static
void
extract_inds(int *inds, const size_t *h, size_t n)
{
	size_t i;
	int t;

	/* Walk through & get hit indices */
	t = 0;
	for (i = 0; i < n; ++i)
		if (h[i] > 0)
			inds[t++] = (int)(i + 1);   /* NB. adjust for R indices */
}

SEXP
poibm_sel_w_R(SEXP w_R, SEXP inds_X_R, SEXP inds_Y_R, SEXP scratch_R)
{
	size_t nx, ny, ux, uy, *hx, *hy, eff_ux, eff_uy, *J;
	SEXP res_R;

	/* Check arguments */
	check_w(&nx, &ny, w_R);
	check_inds(&ux, inds_X_R, nx, "inds.X");
	check_inds(&uy, inds_Y_R, ny, "inds.Y");

	/* Empty problem? */
	if (!(ux > 0  || uy > 0 ))
		goto done;
	/* Full problem? */
	if (!(ux < nx || uy < nx))
		goto done;

	/* Check scratch */
	if (isReal(scratch_R) && (size_t)xlength(scratch_R) * sizeof(*REAL(scratch_R)) >= (nx * ny) * sizeof(*J))
		J = (size_t *)REAL(scratch_R);
	else
		J = (size_t *)R_alloc(sizeof(*J), (int)(nx * ny));

	/* Get scratch */
	hx = (size_t *)alloca(nx * sizeof(*hx));
	hy = (size_t *)alloca(ny * sizeof(*hy));

	/* Update */
	poibm_sel_w(&eff_ux, &eff_uy, hx, hy, REAL(w_R), J, nx, ny, ux, uy);
	extract_inds(INTEGER(inds_X_R), hx, nx);
	extract_inds(INTEGER(inds_Y_R), hy, ny);

	/* Assemble output list */
done:
	res_R = PROTECT(allocList(2));
	SETCAR(res_R, inds_X_R);
	SET_TAG(res_R, install("inds.X"));
	SETCAR(CDR(res_R), inds_Y_R);
	SET_TAG(CDR(res_R), install("inds.Y"));

	UNPROTECT(1);
	return res_R;
}

static
void
pack_w(double *a, size_t nx, size_t ny, const int *inds_X, const int *inds_Y, size_t ux, size_t uy)  
{
	size_t q, j, p, k;

	/* Check for empty input */
	if (!(ux > 0  && uy > 0 ))
		return;
	
	/* Loop through columns */
					q = 0;
	for (j = 0; j < ny; ++j)
		if (j == (size_t)(inds_Y[q] - 1))   /* NB. adjust for R indices */
		{
			/* Loop through rows */
					p = 0;
			for (k = 0; k < nx; ++k)
				if (k == (size_t)(inds_X[p] - 1))
				{
					/* Pack in */
					a[p + q*ux] = a[k + j*nx];

					/* DonE? */
					if (!(++p < ux))
						break;
				}
					/* Done? */
					if (!(++q < uy))
						break;
		}
}

static
void
unpack_w(double *a, size_t nx, size_t ny, const int *inds_X, const int *inds_Y, size_t ux, size_t uy)  
{
	size_t q, j, p, k;

	/* Check for empty input */
	if (!(ux > 0  && uy > 0 ))
		return;

	/* Loop through columns */
					q = uy-1;
	for (j = ny; j-- > 0;)
		if (j == (size_t)(inds_Y[q] - 1))
		{
			/* Loop through rows */
			{{
					p = ux-1;
			for (k = nx; k-- > 0;)
				if (k == (size_t)(inds_X[p] - 1))
				{
					/* Pack in */
					a[k + j*nx] = a[p + q*ux];

					/* Done? */
					if (!(p-- > 0))
						break;
				}
				else
					/* Fill */
					a[k + j*nx] = 0.;
			}}
					/* Fill */
			{{
			for (; k-- > 0;)
					a[k + j*nx] = 0.;
			}}

					/* Done? */
					if (!(q-- > 0))
						break;
		}
		else
					/* Fill */
			for (k = nx; k-- > 0;)
					a[k + j*nx] = 0.;

	{{
	for (; j-- > 0;)
			for (k = nx; k-- > 0;)
					a[k + j*nx] = 0.;
	}}
}

SEXP
poibm_scal_w_R(SEXP w_R, SEXP inds_X_R, SEXP inds_Y_R, SEXP max_iter_R)
{
	size_t nx, ny, ux, uy;
	SEXP nlogl_R, res_R;

	/* Check arguments */
	check_w(&nx, &ny, w_R);
	check_inds(&ux, inds_X_R, nx, "inds.X");
	check_inds(&uy, inds_Y_R, ny, "inds.Y");

	/* Get iteration count */
	if (!(isInteger(max_iter_R) && length(max_iter_R) == 1 && *INTEGER(max_iter_R) >= 0))
		error("'%s' must be a non-negative integer scalar", "max.iter");

	/* Create output */
	nlogl_R = PROTECT(ScalarReal(nan("")));

	/* Pack data consecutively, unless already so  */
	if (ux < nx || uy < ny)
		pack_w(REAL(w_R), nx, ny, INTEGER(inds_X_R), INTEGER(inds_Y_R), ux, uy );

	/* Scale */
	*REAL(nlogl_R) = poibm_scal_w(REAL(w_R), ux, uy, ux, uy, (size_t)*INTEGER(max_iter_R));

	/* Restore positions */
	if (ux < nx || uy < ny)
		unpack_w(REAL(w_R), nx, ny, INTEGER(inds_X_R), INTEGER(inds_Y_R), ux, uy );

	/* Assemble output list */
	res_R = PROTECT(allocList(2));
	SETCAR(res_R, w_R);
	SET_TAG(res_R, install("w"));
	SETCAR(CDR(res_R), nlogl_R);
	SET_TAG(CDR(res_R), install("nlogl"));

	UNPROTECT(2);
	return res_R;
}

SEXP
poibm_map_canon_R(SEXP c_R, SEXP u_R, SEXP v_X_R, SEXP v_Y_R, SEXP w_R)
{
	size_t m, nx, ny;
	SEXP res_R;

	/* Check arguments */
	check_params(&m, &nx, &ny, c_R, u_R, v_X_R, v_Y_R, w_R);

	/* Compute */
	poibm_map_canon(REAL(c_R), REAL(u_R), REAL(v_X_R), REAL(v_Y_R), m, nx, ny);

	/* Assemble output list */
	res_R = PROTECT(allocList(4));
	SETCAR(res_R, c_R);
	SET_TAG(res_R, install("c"));
	SETCAR(CDR(res_R), u_R);
	SET_TAG(CDR(res_R), install("u"));
	SETCAR(CDDR(res_R), v_X_R);
	SET_TAG(CDDR(res_R), install("v.X"));
	SETCAR(CDDDR(res_R), v_Y_R);
	SET_TAG(CDDDR(res_R), install("v.Y"));

	UNPROTECT(1);
	return res_R;
}
