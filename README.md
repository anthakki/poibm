
POIBM -- Poisson batch correction through sample matching
=========================================================

POIBM is a batch factor inference and correction method that is suited for heterogeneous RNA-seq or other count data datasets. It operates by simulataneously inferring the batch factors and a mapping between matching samples. This is advantageous for datasets, which comprise of samples of heterogeneous populations, in which unknown subpopulations match but e.g. the subpopulation fractions vary so the global population statistics cannot be matched.

Major features:

- Simulatenous batch factor and sample matching inference reveals both the batch correction coefficients and putatively similar phenotypes in the data. The phenotypes need not to be prelabeled, but are learned in the process, as this is often difficult in patient derived samples.

- Supports sample trimming for datasets that have only very little overlap

- The model accounts for the discrete nature of RNA-seq data and models both expression and technical noise or the lack of thereof, operates on raw count data, and infers total RNA factors in the process

For the details about the method and validation on cancer cell line and patient data, please refer to our [publication][1] on the matter.

[1]: https://doi.org/10.1093/bioinformatics/btac124


Installation
------------

POIBM is provided as an R package. It can be installed by [standard R mechanisms](https://cran.r-project.org/doc/manuals/r-release/R-admin.html#Installing-packages). Namely, download the source code and build and install the package from the R prompt:

```
	install.packages('r/poibm/', type = 'source', repos = NULL)
```

Or from the UNIX shell:

```
	R CMD INSTALL 'r/poibm/'
```

Or if you have the [`devtools`](https://www.r-project.org/nosvn/pandoc/devtools.html) package, directly using the URL.

In general, you will need [R](https://www.r-project.org/), a C compiler (such as [GCC](https://gcc.gnu.org/) or [clang](https://clang.llvm.org/)), and `alloca.h` (which is non-standard C but provided with most compilers). POIBM has been tested with R 3.6.3 and GCC 9.3.0 on Ubuntu Linux 20.04.3.


General usage
-------------

Generally, in the analysis, two matrices are concerned: let `X` be the matrix representing the data for the target samples, and `Y` the source samples, where rows represent genes, columns represent samples, and the values are read counts.

The batch correction of `Y` into the space of `X`, giving `Y.hat`,  could be done in R as:

```
	library(poibm)
	fit <- poibm.fit(X, Y)
	Y.hat <- poibm.apply(Y, fit)
```

The parameters are documented in the R package documentation, which are available through the `?` operator in R, e.g. `?`[`poibm.fit`](r/poibm/R/poibm.R) and `?`[`poibm.apply`](r/poibm/R/poibm.R).

Some common options are:

- Use `rho` (or `rho.X` and `rho.Y`) for trimming: this might be necessary if the major subtypes in the two populations are unique to each dataset. Trimming should be kept at minimum. 

- Use `max.iter`, `min.tol`, and `max.resets` to control the optimization parameters. Higher number of `max.iter` and `max.resets` increase the computational but might improve the accuracy for complex problems, while `min.tol` does the converse.

- Use `verbose` to print a progress bar

An commented example using simulated data is provided in [`test/example.R`](test/example.R), as described below.


Example analysis
----------------

This analysis can be reproduced using the script [`test/example.R`](test/example.R). The first section of the script generates data in two batches with three distinct phenotypes. By performing principal component analysis, one can verify that batch effects dominate the variation in these data. Here, the marker represent the batches and the colors the phenotypes:

![PCA of raw data](test/example-1.png){ width=250px } []()


Afterwards, POIBM is run on the two batch datasets `X[, K==1]` and `X[, K==2]` in the script, and the latter is transformed into the space of the former, to obtain a harmonized matrix `Y`. As there are major unique subtypes, trimming at 25% with 20 restarts is used. As can be seen in the script, POIBM is blind to the phenotypic information `L`.

By examining the POIBM estimated versus the true parameters, it can be concluded that POIBM can estimate each in an effective and unbiased manner. That is small variation exists, due to the stochastic nature of the data, but this averages out, across the whole parameter domain:

![batch coefficients](test/example-2.png){ width=250px }
![average expression levels](test/example-3.png){ width=250px }
![gain of X](test/example-4.png){ width=250px }
![gain of Y](test/example-5.png){ width=250px }

By examining the mapping weights, it can be concluded that POIBM has successfully recognized the matching phenotypes in the data, while ignoring the subpopulations unique to each of the datasets.

![mapping weight](test/example-6.png){ width=250px }
![mapping weights rendered on PCA](test/example-7.png){ width=250px }

Finally, by performing PCA on the corrected data, it can be concluded that there are total of three phenotypes, and the difference between the unique phenotypes, now free of the batch effects, can be quantied:

![PCA of corrected data](test/example-8.png){ width=250px } []()


Copying
-------

All files are subject to the simplified BSD license. Please refer to [`LICENSE.txt`](LICENSE.txt) for details. Copyright (c) 2021 Antti Hakkinen and Susanna Holmstrom.
